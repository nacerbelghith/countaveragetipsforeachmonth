package com.tcb.CountAverageTipsForEachMonth;

public class Features {
	double TripID ;
	double DropoffDatetime;
	double DropoffLatitude;
	double dropoff_longitude;
	double dropoff_taxizone_id;
	double extra;
	double fare_amount;
	String payment_type;
	long pickup_datetime;
	double pickup_latitude;
	double pickup_longitude;
	double pickup_taxizone_id;
	String store_and_fwd_flag;
	double tip_amount;
	double tolls_amount;
	double total_amount;
	double trip_distance;
	String trip_type;
	String vendor_id;

}
