package com.tcb.CountAverageTipsForEachMonth;

import java.io.IOException;

import org.apache.commons.net.ntp.TimeStamp;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


public class AverageTipsDriver {
	public final static String INPUT_PATH = "hdfs://192.168.1.113:8020/user/ex-hadoop/yellow-taxi.csv";
	public final static String OUTPUT_PATH = "file:///home/nassereddine/workspace/CountAverageTipsForEachMonth/target/";
	public static void main(String[] args) throws IllegalArgumentException, IOException, ClassNotFoundException, InterruptedException {
		
		
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf);
		conf.addResource(new Path("/hadoop/projects/hadoop-2/conf/core-site.xml"));
        conf.addResource(new Path("/hadoop/projects/hadoop-2/conf/hdfs-site.xml"));

        
        
        
        
		FileInputFormat.setInputPaths(job, new Path(INPUT_PATH));
		FileOutputFormat.setOutputPath(job, new Path(OUTPUT_PATH + TimeStamp.getCurrentTime()));
		job.setJarByClass(AverageTipsDriver.class);
		job.setMapperClass(CountAverageTipsMapper.class);

		job.setReducerClass(CountAverageTipsReducer.class);
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(DoubleWritable.class);
		System.exit(job.waitForCompletion(true) ? 0 : 1);
		
		

	}

}
