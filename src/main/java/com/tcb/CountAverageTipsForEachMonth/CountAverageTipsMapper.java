package com.tcb.CountAverageTipsForEachMonth;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class CountAverageTipsMapper extends Mapper<Object, Text, IntWritable, DoubleWritable> {

	@Override
	public void map(Object key, Text value, Mapper<Object, Text, IntWritable, DoubleWritable>.Context context)
			throws IOException, InterruptedException {
		try {
			//We need to escape the first line of the csv file because it is the header 
            if (value.toString().contains("TripID") /*Some condition satisfying it is header*/)
                return;
            else { 
            	String[] line = value.toString().split(",");
            	//TimeConverter timeConverter = new TimeConverter();
            	Features features = new Features();
        		features.pickup_datetime = Long.parseLong(line[8]);

            	int month = TimeConverter.getMonth(features.pickup_datetime);
            	

        		IntWritable outputKey = new IntWritable(month);
        		DoubleWritable outputValue = new DoubleWritable(Double.parseDouble(line[13]));
        		//System.out.println(outputValue);

        		context.write(outputKey, outputValue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

		
	}

}
