package com.tcb.CountAverageTipsForEachMonth;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class CountAverageTipsReducer extends Reducer<IntWritable, DoubleWritable, IntWritable, DoubleWritable> {
	double tipsSum = 0;
	int comp = 0;

	@Override
	public void reduce(IntWritable dateTime, Iterable<DoubleWritable> values,
			Reducer<IntWritable, DoubleWritable, IntWritable, DoubleWritable>.Context context)
			throws IOException, InterruptedException {
		// System.out.println("I am here ");

		for (DoubleWritable value : values) {
			comp++;
			// System.out.println(comp);
			tipsSum += value.get();

		}
		System.out.println(comp);
		double db = tipsSum / 30;
		context.write(dateTime, new DoubleWritable(db));

	}

}
